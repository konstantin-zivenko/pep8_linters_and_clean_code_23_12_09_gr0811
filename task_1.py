# Напишіть програму, яка перевертає всі слова введеного тексту:
# Приклади:
# "" => "", "asdf" => "fdsa", "abcd efgh" => "dcba hgfe"
#
# Усі небуквені символи повинні залишатися на тих самих місцях,
# але якщо символ - десяткова цифра, то він збільшується на одиницю
# ("9" - пертворюється в "0")
# Приклади:
# "129" => "230", "1-^" => "2-^", "a1bcd efg!h" => "d2cba hgf!e"
# Використовуйте лише латинський алфавіт для тесту.
#
# Ви повинні написати функцію, яка повертає зворотній текст


from typing import Iterable


def get_words_list(text: str) -> list[str]:
    """Get from text word list.

    Split text by space symbols.
    """
    return text.split()


def get_reversed_word(word: str) -> str:
    reversed_word = ""
    all_alpha = [symbol for symbol in word if symbol.isalpha()]
    # all_alpha = []
    # for symbol in word:
    #     if symbol.isalpha():
    #         all_alpha.append(symbol)
    for symbol in word:
        if symbol.isalpha():
            reversed_word += all_alpha.pop()
        elif symbol.isdigit():
            reversed_word += str((int(symbol) + 1) % 10)
        else:
            reversed_word += symbol
    return reversed_word


def get_text_from_word_list(word_list: Iterable[str]) -> str:
    return " ".join(word_list)


def revers_tricky(text: str) -> str:
    word_list = get_words_list(text)
    reversed_word_list = map(get_reversed_word, word_list)
    return get_text_from_word_list(reversed_word_list)


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("asdf", "fdsa"),
        ("abcd efgh", "dcba hgfe"),
        ("129", "230"),
        ("1-^", "2-^"),
        ("a1bcd efg!h", "d2cba hgf!e"),
        ("as3\nsd", "sa4 ds"),
    )

    for num, case in enumerate(cases, 1):
        arg, result = case
        assert revers_tricky(arg) == result, (
            f"{num}: expected: revers_tricky('{arg}') == '{result}',/"
            f" but got: revers_tricky('{arg}') = '{revers_tricky(arg)}'"
        )
